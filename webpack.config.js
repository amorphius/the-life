const path = require("path");

module.exports = {
  entry: "./src/index.js",
  output: {
    filename: "app.js",
  },
  module: {
    rules: [
      {
        test: /.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: 'file-loader',
          },
        ],
      },
    ]
  },
  resolve: {
    extensions: [".js", ".jsx"],
    alias: {
      components: path.resolve(__dirname, "src/components/"),
      utils$: path.resolve(__dirname, "src/utils.js"),
    },
  },
  devServer: {
    host: "0.0.0.0",
    hot: true,
    open: true,
    port: 9090,
  }
}
