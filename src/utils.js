// @flow

export function createEmptyBoard() {
  let board: Array<string> = new Array(50).fill("");
  board = board.map(r => new Array(50).fill("0").join(""));
  return board;
}

export function generateRandom() {
  const board = []
  for (let i = 0; i < 50; i++) {
    for (let j = 0; j < 50; j++) {
      if (!j) {
        board[i] = ""
      }
      const rnd = Math.round(Math.random());
      board[i] = board[i].concat(rnd.toString());
    }
  }

  return board;
}

export function replaceChar(str: string, index: number, char: ?string, isInvert: ?boolean) {
  const head = str.slice(0, index);
  const tail = str.slice(index + 1);
  if (isInvert) {
    char = str.substr(index, 1);
    return head + (char === "1" ? "0" : "1") + tail;
  } else {
    return head + (char || "") + tail;
  }
}

export function getNearest(board: Array<string>, i: number, j: number) {
  let res = [];

  // top
  if (i > 0) {
    // top left
    if (j > 0) {
      res.push(board[i - 1][j - 1]);
    }

    res.push(board[i - 1][j]);

    // top right
    if (j < 49) {
      res.push(board[i - 1][j + 1]);
    }
  }

  // left
  if (j > 0) {
    res.push(board[i][j - 1]);
  }

  // right
  if (j < 49) {
    res.push(board[i][j + 1]);
  }

  // bottom
  if (i < 49) {
    // bottom left
    if (j > 0) {
      res.push(board[i + 1][j - 1]);
    }

    res.push(board[i + 1][j]);

    // bottom right
    if (j < 49) {
      res.push(board[i + 1][j + 1]);
    }
  }

  return res;
}

export function merge(src: string, dest: Array<string>, i: number, j: number) {
  const len = src.length;
  const head = dest[i].slice(0, j);
  const tail = dest[i].slice(j + len);
  const row = head + src + tail;
  dest[i] = row;
}