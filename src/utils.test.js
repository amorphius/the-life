import { getNearest, createEmptyBoard, merge } from "./utils";

it("should return nearest in the middle of the board", () => {
  const board = createEmptyBoard();
  merge("123", board, 10, 10);
  merge("456", board, 11, 10);
  merge("789", board, 12, 10);
  const nearest = getNearest(board, 11, 11);
  expect(nearest).toEqual(["1", "2", "3", "4", "6", "7", "8", "9"]);
});

it("should return nearest in the bottom right corner", () => {
  const board = createEmptyBoard();
  merge("qw", board, 48, 48);
  merge("xz", board, 49, 48);
  const nearest = getNearest(board, 49, 49);
  expect(nearest).toEqual(["q", "w", "x"]);
});


it("should return nearest on the left edge", () => {
  const board = createEmptyBoard();
  merge("rt", board, 40, 0);
  merge("fg", board, 41, 0);
  merge("vb", board, 42, 0);
  const nearest = getNearest(board, 41, 0);
  expect(nearest).toEqual(["r", "t", "g", "v", "b"]);
});
