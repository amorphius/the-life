import React from "react";
import { BoardLayout, Row, Cell, Circle } from "./Board.styled";

const Board = ({ rows, onCellClick }) => {

  function renderCell(cell, i, j) {
    return (
      <Cell key={j} onClick={() => onCellClick(i, j)}>
        {cell === "1" && (
          <Circle />
        )}
      </Cell>
    )
  }

  return (
    <BoardLayout>
      {rows.map((row, i) => (
        <Row className="row" key={i}>
          {row.split("").map((cell, j) => renderCell(cell, i, j))}
        </Row>
      ))}
    </BoardLayout>
  )
}

export default Board;