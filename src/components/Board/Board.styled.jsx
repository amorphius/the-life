import styled from "styled-components";

export const BoardLayout = styled.div`
  margin-top: 1rem;
  border-top: 1px solid #ccc;
  border-left: 1px solid #ccc;
  width: 500px;
  height: 500px;
  display: flex;
  flex-direction: column;
`

export const Row = styled.div`
  display: flex;
`

export const Cell = styled.div`
  border-right: 1px solid #ccc;
  border-bottom: 1px solid #ccc;
  width: 10px;
  height: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const Circle = styled.div`
  width: 100%;
  height: 100%;
  background: black;
  border-radius: 1rem;
`