// @flow
import React, { useState, useContext } from "react";
import { ControlsLayout, Speed, Button, Buttons, SpeedLabel, StepLabel } from "./Controls.styled";
import { AppContext } from "components/App";

type PropsType = {
  onClear: () => mixed,
  onRandom: () => mixed,
  onStartStop: () => mixed,
  onSpeedChange: (number) => mixed,
  onNext: () => mixed,
  step: number,
}

const Controls = (props: PropsType) => {
  const {
    onClear,
    onRandom,
    onStartStop,
    onSpeedChange,
    onNext,
    step } = props;
  const { tick, started, speed } = useContext(AppContext);
  const [percent: number, setPercent] = useState(25);

  function onSpeed(percent: number) {
    setPercent(percent);
    const formula = (t, a = 0, b = 100, c = 0.25, d = 4) => c + ((d - c) / (b - a)) * (t - a);
    const raw: number = formula(+percent);
    let newSpeed: number;
    if (raw > 1) {
      newSpeed = Math.floor(raw);
    } else {
      newSpeed = Math.floor(raw * 4) / 4;
    }

    if (newSpeed !== speed) {
      onSpeedChange(newSpeed);
    }
  }

  return (
    <ControlsLayout>
      <Speed
        defaultValue={25}
        value={percent}
        onChange={onSpeed}
      />
      <SpeedLabel>
        Speed: {speed}x
      </SpeedLabel>

      <Buttons>
        <StepLabel>
          Current step: {step}
        </StepLabel>
        <Button mod="next" onClick={onNext}>
          Next
        </Button>
      </Buttons>

      <Buttons>
        <Button className={started ? "mod-stop" : "mod-start"} onClick={onStartStop}>
          {started ? "Stop" : "Start"}
        </Button>
        <Button onClick={onClear}>
          Clear
        </Button>
        <Button onClick={onRandom}>
          Random
        </Button>
      </Buttons>
    </ControlsLayout>
  )
}

export default Controls;
