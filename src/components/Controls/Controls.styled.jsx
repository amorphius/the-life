import styled from "styled-components";
import Slider from "react-slider-simple";

export const ControlsLayout = styled.div`
  display: flex;
  flex-direction: column;
  align-items: stretch;
  width: 500px;
  margin-top: 1rem;
`

export const Speed = styled(Slider)``

export const Buttons = styled.div`
  margin-top: 0.5rem;
  display: flex;
  justify-content: space-around;
`

export const Button = styled.button`
  border: 2px solid #777;
  background: white;
  width: 10rem;
  height: 2rem;
  outline: none;
  border-radius: 0.3rem;
  margin: 0.5rem;
  cursor: pointer;
  transition: padding-bottom 0.2s ease;
  padding-bottom: 0;
  font-size: 1rem;
  font-weight: 500;
  color: #333;
  flex-grow: ${p => (p.mod === "next" ? "" : "99")};

  &:hover {
    padding-bottom: 0.2rem;
  }

  &.mod-start {
    background: green;
    color: white;
  }

  &.mod-stop {
    background: red;
    color: white;
  }
`

export const SpeedLabel = styled.div`
  flex-grow: 99;
  text-align: center;
`

export const StepLabel = styled.div`
  align-self: center;
  margin-left: 1rem;
  margin-right: 1rem;
`