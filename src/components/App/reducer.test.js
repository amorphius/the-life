import reducer from "./reducer";
import { createEmptyBoard, merge } from "../../utils";

it("should produce two cells on second tick", () => {
  const board = createEmptyBoard();
  merge("111", board, 0, 10);

  const result = createEmptyBoard();
  merge("1", result, 0, 11);
  merge("1", result, 1, 11);

  const state = reducer({
    rows: board,
  }, {
    type: "TICK"
  });

  expect(state.rows).toEqual(result);
});

it("should not produce any cells on empty board", () => {
  const empty = createEmptyBoard();

  const state = reducer({
    rows: empty
  }, {
    type: "TICK"
  });

  expect(state.rows).toEqual(empty);
});

it("should produce empty board because of overpopulation", () => {
  const full = new Array(50).fill(new Array(50).fill("1").join(""));
  const corners = createEmptyBoard();
  merge("1", corners, 0, 0);
  merge("1", corners, 0, 49);
  merge("1", corners, 49, 0);
  merge("1", corners, 49, 49);
  const empty = createEmptyBoard();

  const tick1 = reducer({
    rows: full
  }, {
    type: "TICK"
  });
  expect(tick1.rows).toEqual(corners);

  const tick2 = reducer({
    rows: tick1.rows
  }, {
    type: "TICK"
  });
  expect(tick2.rows).toEqual(empty);
});
