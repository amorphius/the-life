// @flow
import React, { useState, useEffect, useCallback, useReducer, useContext, createContext } from "react";
import Board from "components/Board";
import Controls from "components/Controls";
import Conway from "components/Conway";
import reducer, { initial } from "./reducer";
import { AppLayout, Global, Root } from "./App.styled";
import { generateRandom } from "utils";

type AppContextType = {}
export const AppContext = createContext<AppContextType>({});

const App = () => {
  const [state, dispatch] = useReducer(reducer, initial);
  const [timer: number, setTimer] = useState(0);

  useEffect(() => {
    dispatch({
      type: "SET_ROWS",
      rows: generateRandom(),
    });
  }, []);

  function stop() {
    if (timer) {
      clearInterval(timer);
      setTimer(0);
    }

    dispatch({
      type: "TOGGLE_STARTED",
      value: false,
    });
  }

  const onCellClick = useCallback((i: number, j: number) => {
    dispatch({
      type: "TOGGLE_CELL",
      i,
      j,
    })
  }, []);

  const onClear = useCallback(() => {
    dispatch({
      type: "CLEAR"
    });
    stop();
  }, [timer]);

  const onRandom = useCallback(() => {
    dispatch({
      type: "SET_ROWS",
      rows: generateRandom(),
    });
    stop();
  }, [timer]);

  const onStartStop = useCallback(() => {
    dispatch({
      type: "TOGGLE_STARTED",
    });
    if (!state.started) {
      const id = setInterval(() => {
        dispatch({
          type: "TICK",
        });
      }, 1 / state.speed * 1000);
      setTimer(id);
    } else {
      if (timer) {
        clearInterval(timer);
        setTimer(0);
      }
    }
  }, [timer, state.started, state.speed]);

  const onSpeedChange = useCallback((speed: number) => {
    dispatch({
      type: "SET_SPEED",
      speed,
    });

    if (timer) {
      clearInterval(timer);
      const id = setInterval(() => {
        dispatch({
          type: "TICK",
        });
      }, 1 / speed * 1000);
      setTimer(id);
    }
  }, [timer, state.started]);

  const onNext = useCallback(() => {
    dispatch({
      type: "TICK",
    });
  }, []);

  return (
    <Root>
      <Global />
      <AppContext.Provider value={state}>
        <AppLayout>
          <Board
            rows={state.rows}
            onCellClick={onCellClick}
          />
          <Controls
            started={state.started}
            onClear={onClear}
            onRandom={onRandom}
            onStartStop={onStartStop}
            onSpeedChange={onSpeedChange}
            onNext={onNext}
            step={state.step}
          />
        </AppLayout>
      </AppContext.Provider>
      <Conway />
    </Root>
  )
}

export default App;
