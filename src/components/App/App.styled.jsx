import styled, { createGlobalStyle } from "styled-components";

export const Global = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Roboto+Condensed:400,500');

  * {
    box-sizing: border-box;
    font-family: Roboto, "Open Sans", sans-serif;
  }

  body {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`

export const AppLayout = styled.div``

export const Root = styled.div`
  display: flex;
`
