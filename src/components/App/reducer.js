// @flow
import { replaceChar, getNearest } from "../../utils";

type StateType = {
  rows: Array<string>,
  speed: number,
  step: number,
  started: boolean,
}

type Actions = "SET_ROWS" | "TOGGLE_CELL" | "CLEAR" | "SET_SPEED" | "TICK" | "SET_SPEED" | "TOGGLE_STARTED";

type ActionType = {
  type: Actions,
  value: ?boolean,
  speed: ?number,
  i: ?number,
  j: ?number,
  rows: ?Array<string>,
}

export const initial: StateType = {
  rows: [],
  speed: 1,
  step: 0,
  started: false,
}

export default (state: StateType, action: ActionType): StateType => {
  let next: Array<string>;

  switch (action.type) {
    case "SET_ROWS":
      return {
        ...state,
        rows: action.rows || [],
        started: false,
        step: 0,
      }
    case "TOGGLE_CELL":
      next = [...state.rows];
      next[action.i] = replaceChar(next[action.i], action.j, null, true);
      return {
        ...state,
        rows: next,
        started: false,
      }
    case "CLEAR":
      next = state.rows.map(r => new Array(50).fill("0").join(""));

      return {
        ...state,
        rows: next,
        started: false,
        step: 0,
      }
    case "SET_SPEED":
      return {
        ...state,
        speed: action.speed || 0,
      }
    case "TICK":
      const newRows = state.rows.map((r, i) => {
        return r.split("").map((c, j) => {
          const nearest = getNearest(state.rows, i, j);
          const sum = nearest.reduce((m, x) => +m + +x);
          if (sum === 3) {
            return 1;
          } else if (sum === 2) {
            return c
          } else {
            return 0;
          }
        }).join("")
      });

      return {
        ...state,
        step: state.step + 1,
        rows: newRows,
      }
    case "TOGGLE_STARTED":
      if (action.value != null) {
        return {
          ...state,
          started: action.value,
        }
      } else {
        return {
          ...state,
          started: !state.started,
        }
      }

    default:
      return state
  }
}

