import React from "react";
import { ConwayLayout, Img, Label } from "./Conway.styled";
import photo from "../../../conway.jpg"

const Conway = () => {
  return (
    <ConwayLayout>
      <Img src={photo} />
      <Label>
        In memory of John Conway
      </Label>
    </ConwayLayout>
  )
}

export default Conway;