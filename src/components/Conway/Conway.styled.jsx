import React from "react";
import styled from "styled-components";

export const ConwayLayout = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-left: 3rem;
  margin-top: 4rem;
`
export const Img = styled.img`
  width: 15rem;
  border: 0.2rem solid #111;
  border-radius: 0.2rem;
  padding: 0.3rem;
`

export const Label = styled.div`
  margin-top: 0.7rem;
  font-style: italic;
  font-size: 0.8rem;
`